import axios from 'axios';
import Vue from 'vue';
import App from './App.vue';
import store from './store';
import './registerServiceWorker';

Vue.prototype.$axios = axios;
Vue.prototype.$eventBus = new Vue();
Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
