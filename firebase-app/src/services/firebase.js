import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCfJf9xAQgV4r0ydK9-XaBqs_SSanqxRoA',
  authDomain: 'maikzn-fx.firebaseapp.com',
  databaseURL: 'https://maikzn-fx.firebaseio.com',
  projectId: 'maikzn-fx',
  storageBucket: 'maikzn-fx.appspot.com',
  messagingSenderId: '607625152313',
  appId: '1:607625152313:web:6b90a8ebe36157a16e4ae8',
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

// make auth and firestore references
export const auth = firebase.auth();
export const db = firebase.firestore();
export const firebaseAuth = firebase.auth;
export const storage = firebase.storage();
